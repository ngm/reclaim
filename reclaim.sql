-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 08, 2023 at 06:00 PM
-- Server version: 8.0.32-0ubuntu0.20.04.2
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reclaim`
--

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `name`, `description`) VALUES
(1, 'planetary stability', ''),
(2, 'social equity', ''),
(3, 'agency', '');

-- --------------------------------------------------------

--
-- Table structure for table `examples`
--

CREATE TABLE `examples` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `solution_id` int NOT NULL,
  `lever_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `examples`
--

INSERT INTO `examples` (`id`, `name`, `description`, `solution_id`, `lever_id`) VALUES
(1, 'DECODE', '(I think it\'s a data commons)', 1, 3),
(2, 'Decidim', '', 13, 3),
(3, 'Loomio', '', 13, 3),
(4, 'Polis', '', 13, 3),
(5, 'right to repair', '', 4, 2),
(6, 'fediverse', '', 16, 3),
(7, 'indieweb', '', 16, 3),
(8, 'Debian', '', 17, 3),
(9, 'adversarial interoperability', '', 16, 1),
(10, 'community repair', '', 4, 1),
(11, 'fairphone', '', 4, 3),
(12, 'wikipedia', '', 12, 3),
(13, 'abolition of intellectual property', '', 12, 2),
(14, 'freedombox', '', 18, 3),
(15, 'open street map', '', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `layers`
--

CREATE TABLE `layers` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `layers`
--

INSERT INTO `layers` (`id`, `name`, `description`) VALUES
(1, 'network', ''),
(2, 'hardware', ''),
(3, 'software', '');

-- --------------------------------------------------------

--
-- Table structure for table `levers`
--

CREATE TABLE `levers` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `levers`
--

INSERT INTO `levers` (`id`, `name`, `description`) VALUES
(1, 'resist', ''),
(2, 'regulate', ''),
(3, 'recode', '');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE `problems` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `layer_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`id`, `name`, `description`, `layer_id`) VALUES
(1, 'privately owned networks', '', 1),
(2, 'polluting data centres', '', 1),
(3, 'early obsolescence', '', 2),
(4, 'device over consumption', '', 2),
(5, 'californian ideology', '', 3),
(6, 'proprietary knowledge', '', 3),
(7, 'big tech platforms', '', 3),
(8, 'data surveillance', '', 3),
(9, 'closed social media', '', 3),
(10, 'proprietary operating systems', '', 3),
(11, 'digital divide', '', 2),
(12, 'centralised hosting', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `problem_examples`
--

CREATE TABLE `problem_examples` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `problem_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `problem_examples`
--

INSERT INTO `problem_examples` (`id`, `name`, `description`, `problem_id`) VALUES
(1, 'microsoft windows', '', 10),
(2, 'mac osx', '', 10),
(3, 'facebook', '', 9),
(4, 'twitter', '', 9),
(5, 'amazon', '', 7),
(6, 'alphabet', '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `solutions`
--

CREATE TABLE `solutions` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `layer_id` int DEFAULT NULL,
  `problem_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `solutions`
--

INSERT INTO `solutions` (`id`, `name`, `description`, `layer_id`, `problem_id`) VALUES
(1, 'data commons', '', 3, 8),
(2, 'platform coops', '', 3, 7),
(3, 'community broadband', '', 1, 1),
(4, 'repairable, durable devices', '', 2, 3),
(5, 'digital sufficiency', '', 2, NULL),
(6, 'n digital divide', '', 2, NULL),
(7, 'community isps', '', 1, NULL),
(8, 'community mesh networks', '', 1, 1),
(9, 'municipal connectivity', '', 1, 1),
(10, 'renewable energy data centres?', '', 1, 2),
(11, 'public good submarine cables', '', 1, NULL),
(12, 'knowledge commons', '', 3, 6),
(13, 'participatory budget / planning tools', '', 3, NULL),
(15, 'data trusts', '', 3, 8),
(16, 'alternative social media', '', 3, 9),
(17, 'libre operating systems', '', 3, 10),
(18, 'community hosting', '', 2, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examples`
--
ALTER TABLE `examples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `solution` (`solution_id`),
  ADD KEY `lever` (`lever_id`) USING BTREE;

--
-- Indexes for table `layers`
--
ALTER TABLE `layers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levers`
--
ALTER TABLE `levers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `layer` (`layer_id`);

--
-- Indexes for table `problem_examples`
--
ALTER TABLE `problem_examples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `problem` (`problem_id`);

--
-- Indexes for table `solutions`
--
ALTER TABLE `solutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `problem` (`problem_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `examples`
--
ALTER TABLE `examples`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `layers`
--
ALTER TABLE `layers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `levers`
--
ALTER TABLE `levers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `problem_examples`
--
ALTER TABLE `problem_examples`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `solutions`
--
ALTER TABLE `solutions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `examples`
--
ALTER TABLE `examples`
  ADD CONSTRAINT `examples_ibfk_1` FOREIGN KEY (`solution_id`) REFERENCES `solutions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `examples_ibfk_2` FOREIGN KEY (`lever_id`) REFERENCES `levers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `problems`
--
ALTER TABLE `problems`
  ADD CONSTRAINT `problems_ibfk_1` FOREIGN KEY (`layer_id`) REFERENCES `layers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `problem_examples`
--
ALTER TABLE `problem_examples`
  ADD CONSTRAINT `problem_examples_ibfk_1` FOREIGN KEY (`problem_id`) REFERENCES `problems` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `solutions`
--
ALTER TABLE `solutions`
  ADD CONSTRAINT `solutions_ibfk_1` FOREIGN KEY (`problem_id`) REFERENCES `problems` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
