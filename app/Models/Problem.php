<?php

namespace App\Models;

use App\Models\ProblemExample;
use App\Models\Solution;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Problem extends Model
{
    use HasFactory;

    public function examples(): HasMany
    {
        return $this->hasMany(ProblemExample::class);
    }

    public function solutions(): HasMany
    {
        return $this->hasMany(Solution::class);
    }
}
