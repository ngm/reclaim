<?php

namespace App\Models;

use App\Models\Example;
use App\Models\Problem;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Solution extends Model
{
    use HasFactory;

    public function examples(): HasMany
    {
        return $this->hasMany(Example::class);
    }

    public function problem(): BelongsTo
    {
        return $this->belongsTo(Problem::class);
    }

    public function link()
    {
        return "https://commonplace.doubleloop.net/" . Str::slug($this->name);
    }
}
