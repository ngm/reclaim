<?php

namespace App\Models;

use App\Models\Lever;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

class Example extends Model
{
    use HasFactory;

    public function lever(): BelongsTo
    {
        return $this->belongsTo(Lever::class);
    }

    public function solution() : BelongsTo
    {
        return $this->belongsTo(Solution::class);
    }

    public function link()
    {
        return "https://commonplace.doubleloop.net/" . Str::slug($this->name);
    }
}
