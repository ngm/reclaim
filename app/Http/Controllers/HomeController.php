<?php

namespace App\Http\Controllers;

use App\Models\Example;
use App\Models\Layer;
use App\Models\Level;
use App\Models\Problem;
use App\Models\Solution;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index()
    {
        $layers = Layer::all();

        return view('home', ['layers' => $layers]);
    }

    public function random()
    {
        $initiative = Example::inRandomOrder()->first();

        return view('random', ['initiative' => $initiative]);
    }
}
