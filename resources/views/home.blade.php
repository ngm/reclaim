<style>
    html {
        font-family: sans;
        color: #222;
    }
    .problems-solutions {
        display: grid;
        grid-template-columns: 1fr 1fr 2fr;
        border: 1px solid gray;
        padding: 2px;
    }
    .initiatives {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
    }
    .problems {
        background-color: lightsalmon;
        padding: 10px;
        margin-right: 10px;
    }
    .solutions {
        background-color: lightgreen;
        padding: 10px;
        margin-right: 10px;
    }
    #table a {
        color: #222;
        text-decoration: none;
    }
    #table a:hover {
        text-decoration: underline;
    }
    #header {
        font-weight: bold;
    }
</style>
<h1>reclaim the stacks</h1>

<p>
    For a bit more context: <a href="https://commonplace.doubleloop.net/reclaiming-the-stacks-ecosocialism-and-ict">read this</a>.
</p>

<p>
    Show a <a href="/random">random initiative</a>
</p>

<div id="table">
    <div id="header" class="problems-solutions">
        <div>digital capitalism</div>
        <div>digital ecosocialism</div>
        <div class="initiatives">
            <div>resist</div>
            <div>regulate</div>
            <div>recode</div>
        </div>
    </div>
    @foreach ($layers as $layer)
        <div class="layer">
            <h2>{{ $layer->name }}</h2>
                @foreach ($layer->problems as $problem)
                    <div class="problems-solutions">
                        <div class="problems">
                            <div>{{ $problem->name }}</div>
                            @foreach ($problem->examples as $example)
                                <div>- {{$example->name}}</div>
                            @endforeach
                        </div>
                        <div class="solutions">
                            @if ($problem->solutions)
                                @foreach ($problem->solutions as $solution)
                                    <div>
                                        <a target="_blank" href="{{ $solution->link() }}">{{ $solution->name }}</a>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="initiatives">
                            @if ($problem->solutions)
                                @foreach ($problem->solutions as $solution)
                                <div>
                                @foreach ($solution->examples->where('lever_id', 1) as $example)
                                    <div>
                                        <a target="_blank" href="{{ $example->link() }}">{{ $example->name }}</a>
                                    </div>
                                @endforeach
                                </div>
                                <div>
                                @foreach ($solution->examples->where('lever_id', 2) as $example)
                                    <div>
                                        <a target="_blank" href="{{ $example->link() }}">{{ $example->name }}</a>
                                    </div>
                                @endforeach
                                </div>
                                <div>
                                @foreach ($solution->examples->where('lever_id', 3) as $example)
                                    <div>
                                        <a target="_blank" href="{{ $example->link() }}">{{ $example->name }}</a>
                                    </div>
                                @endforeach
                                </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
        </div>
    @endforeach
</div>
