<style>
    html {
        font-family: sans;
    }
    .initiative {
        margin: auto 20%;
        text-align: center;
        font-size: 40px;
    }
</style>
<h1>reclaim the stacks</h1>
<p>
    <a href="/">Home</a>.
</p>
@php
$name = "";
if ($initiative->lever->name == "resist") $name = "resistance-based";
if ($initiative->lever->name == "regulate") $name = "regulation-based";
if ($initiative->lever->name == "recode") $name = "recoding-based";
@endphp
<div class="initiative">
    <p>support <a href="{{ $initiative->link() }}">{{ $initiative->name }}</a></p>
    <p>a {{ $name }} initiative for <br/>{{ $initiative->solution->name }}</p>
@if ($initiative->solution->problem)
<p>
an alternative to {{ $initiative->solution->problem->name }}
</p>
</div>
@endif
